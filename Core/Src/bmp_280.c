/**
 ****************************************************************************************************************
 * @file 	bmp_280.c
 * @author 	Dilan Becerra (dilan.becerra@utp.edu.co)
 * @brief 	This file contains the functions to communicate whit a bmp380
 * @version 1.0.0
 * @date 	June 4, 2023
 ****************************************************************************************************************
 */

/** Includes --------------------------------------------------------------------------------------------------*/
#include "bmp_280.h"

/** Private defines -------------------------------------------------------------------------------------------*/
/** Private typedef -------------------------------------------------------------------------------------------*/
/** Private macro ---------------------------------------------------------------------------------------------*/
/** Private function prototypes -------------------------------------------------------------------------------*/
/** Private constant definitions ------------------------------------------------------------------------------*/
/** Private variable declaration ------------------------------------------------------------------------------*/
bmp_280_driver_t bmp280_driver;
int SC_BMP280_Pin;

/** Function definition ---------------------------------------------------------------------------------------*/

void bmp_280_driver_init(SPI_HandleTypeDef bmp280_SPI_com, GPIO_TypeDef *bmp280_SS_port, uint16_t bmp280_SS_pin)
{
	bmp280_driver.bmp_280_SPI=bmp280_SPI_com;
	bmp280_driver.SS_port=bmp280_SS_port;
	bmp280_driver.SS_pin=bmp280_SS_pin;
	bmp_write_register(BMP280_REG_ADDR_CTRL_MEAS, 0b00100111); 	//001 min oversampling in T and P for more variation, 11 in LSB is normal mode
	bmp_write_register(BMP280_REG_ADDR_CONFIG, 0b10000000);		//100 in MSB is 500ms standby for least 1 sampling per second, 000 IIR for more variation
}

float bmp_280_get_temperature()
{
	uint8_t dataT[3];
	bmp280_read_register(BMP280_REG_ADDR_TEMP_MSB, &dataT[0]);
	int32_t rawTemp=bmp280_get_raw_TorP(&dataT[0]);
	bmp280_driver.current_temp=bmp280_convert_temperature_to_degrees(rawTemp);
	return bmp280_driver.current_temp;
}

float bmp_280_get_pressure()
{
	uint8_t dataP[3];
	bmp280_read_register(BMP280_REG_ADDR_PRESS_MSB, &dataP[0]);
	int32_t rawPress=bmp280_get_raw_TorP(&dataP[0]);
	bmp280_driver.current_press=bmp280_convert_pressure_to_pascals(rawPress, bmp280_driver.current_temp);
	return bmp280_driver.current_press;
}

void bmp_280_read()
{
	bmp_280_get_temperature();
	bmp_280_get_pressure();
}

HAL_StatusTypeDef bmp_write_register(uint8_t sensor_register, uint8_t value){
	HAL_StatusTypeDef ret_val;
	uint8_t data_to_write[2] = {sensor_register&0b01111111, value};
	HAL_GPIO_WritePin(bmp280_driver.SS_port, bmp280_driver.SS_pin, GPIO_PIN_RESET);
	ret_val=HAL_SPI_Transmit(&bmp280_driver.bmp_280_SPI, data_to_write, 2, 1);
	HAL_GPIO_WritePin(bmp280_driver.SS_port, bmp280_driver.SS_pin, GPIO_PIN_SET);
	return(ret_val);
}

HAL_StatusTypeDef bmp280_read_register(uint8_t sensor_register, uint8_t *value){
	HAL_StatusTypeDef ret_val=HAL_ERROR;
	//uint8_t data_to_write[2] = {sensor_register, value};
	HAL_GPIO_WritePin(bmp280_driver.SS_port, bmp280_driver.SS_pin, GPIO_PIN_RESET);
	if(HAL_SPI_Transmit(&bmp280_driver.bmp_280_SPI, &sensor_register, 1, 1)==HAL_OK){
		ret_val=HAL_SPI_Receive(&bmp280_driver.bmp_280_SPI, value, 1, 1);
	}
	HAL_GPIO_WritePin(bmp280_driver.SS_port, bmp280_driver.SS_pin, GPIO_PIN_SET);
	return(ret_val);
}

int32_t bmp280_get_raw_TorP(uint8_t *BMPregisters){
	return((int32_t)((BMPregisters[0]<<12)|(BMPregisters[1]<<4)|(BMPregisters[2]>>4)));
}

float bmp280_convert_temperature_to_degrees(int32_t raw_temp){
	float var1=0;
	float var2=0;

	var1=(float)((raw_temp)/16384.0-(27504)/1024.0)*(26435);
	var2=(float)(raw_temp/131072.0-27504/8192.0);
	var2=var2*var2;
	var2=var2*(-1000);
	return((float)(var1+var2)/5120.0);
}

float bmp280_convert_pressure_to_pascals(int32_t raw_pressure, float temp_in_degrees){
	float var1=0;
	float var2=0;
	float p=0;

	var1=(temp_in_degrees*5120.0/2.0)-64000.0;
	var2=var1*var1*(-7)/32768.0;
	var2=var2+(var1*(140)*2.0);
	var2=(var2/4.0)+((2855)*65536.0);
	var1=(((3024)*var1*var1/524288.0)+((-10684)*var1))/524288.0;
	var1=(1.0+(var1/32768.0))*(36477);
	p=1048576.0-raw_pressure;
	p=(p-(var2/4096.0))*(6250.0/var1);
	var1=(6000)*p*p/2147483648.0;
	var2=p*(-14600)/32768.0;
	p=p+(var1+var2+(15500))/16.0;
	return(p);
}
