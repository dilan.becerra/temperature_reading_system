/**
 ****************************************************************************************************************
 * @file 	command_parser.c
 * @author 	Dilan Becerra (dilan.becerra@utp.edu.co)
 * @brief 	This file contains the command parser for a UART communication
 * @version 1.0.0
 * @date 	May 26, 2023
 ****************************************************************************************************************
 */

/** Includes --------------------------------------------------------------------------------------------------*/
#include <string.h>
#include "command_parser.h"
#include "uart_driver.h"
#include "main.h"
#include "bmp_280.h"

/** Private defines -------------------------------------------------------------------------------------------*/
#define FW_VERSION "1.0.20230528"
/** Private typedef -------------------------------------------------------------------------------------------*/
/** Private macro ---------------------------------------------------------------------------------------------*/
/** Private function prototypes -------------------------------------------------------------------------------*/
/** Private constant definitions ------------------------------------------------------------------------------*/
const uint8_t ack_message[]		=	"*ACK#";
const uint8_t nack_message[]	=	"*NACK#";

/** External variable declaration -----------------------------------------------------------------------------*/
extern uart_driver_t uart_driver;
extern TIM_HandleTypeDef htim6;

/** Private variable declaration ------------------------------------------------------------------------------*/
uint64_t runtime=0;

/** Function definition ---------------------------------------------------------------------------------------*/

void get_runtime_DHMS(uint8_t *pTime)
{
	uint64_t time_buffer=0;
	time_buffer=(uint64_t)__HAL_TIM_GET_COUNTER(&htim6)+runtime;
	time_buffer=time_buffer/10000;
	pTime[3]=(uint8_t)(time_buffer%60);
	time_buffer=time_buffer/60;
	pTime[2]=(uint8_t)(time_buffer%60);
	time_buffer=time_buffer/60;
	pTime[1]=(uint8_t)(time_buffer%24);
	time_buffer=time_buffer/24;
	pTime[0]=(uint8_t)time_buffer;
}

uint8_t process_set_command(rx_packet_t *rx_packet)
{
	uint8_t ret_val=1;
	switch(rx_packet->element)
	{
		case ELEMENT_FAN_SPEED:
			if((rx_packet->value>='0')&&(rx_packet->value<'5'))
			{
				TIM1->CCR4=(rx_packet->value-48)*25;
			}
			break;
		case ELEMENT_DOOR:
			switch(rx_packet->value)
			{
				case '0':
					HAL_GPIO_WritePin(DOOR_GPIO_Port, DOOR_Pin, GPIO_PIN_RESET);
					break;
				case '1':
					HAL_GPIO_WritePin(DOOR_GPIO_Port, DOOR_Pin, GPIO_PIN_SET);
					break;
				default:
					ret_val=0;
					break;
			}
			break;
		default:
			ret_val=0;
			break;
	}
	return ret_val;
}

uint8_t process_get_command(rx_packet_t *rx_packet)
{
	uint8_t ret_val=1;
	char response_message[20];
	response_message[0]=PROTOCOL_PREAMBLE;
	switch(rx_packet->element)
	{
		case ELEMENT_TEMPERATURE:
			response_message[1]=(char)ELEMENT_TEMPERATURE;
			sprintf(&response_message[2], "%.*f", 2, bmp_280_get_temperature());
			strcat(response_message, "#");
			uart_driver_send(&uart_driver, (uint8_t *)response_message, strlen(response_message));
			break;
		case ELEMENT_FAN_SPEED:
			response_message[1]='F';
			response_message[2]=(TIM1->CCR4/100)+48;
			response_message[3]=((TIM1->CCR4%100)/10)+48;
			response_message[4]=(TIM1->CCR4%10)+48;
			response_message[5]=PROTOCOL_POSAMBLE;
			uart_driver_send(&uart_driver, (uint8_t *)response_message, 6);
			break;
		case ELEMENT_FAN_STATUS:
			response_message[1]=(char)ELEMENT_FAN_STATUS;
			if(TIM1->CCR4==0)
			{
				response_message[2]='0';
			}else{
				response_message[2]='1';
			}
			response_message[3]=PROTOCOL_POSAMBLE;
			uart_driver_send(&uart_driver, (uint8_t *)response_message, 4);
			break;
		case ELEMENT_DOOR:
			response_message[1]=(char)ELEMENT_DOOR;
			response_message[2]=48+HAL_GPIO_ReadPin(DOOR_GPIO_Port, DOOR_Pin);
			response_message[3]=PROTOCOL_POSAMBLE;
			uart_driver_send(&uart_driver, (uint8_t *)response_message, 4);
			break;
		case ELEMENT_FW_VERSION:
			response_message[1]='V';
			strcat(response_message, FW_VERSION);
			strcat(response_message, "#");
			uart_driver_send(&uart_driver, (uint8_t *)response_message, strlen(response_message));
			break;
		case ELEMENT_UNIT_TIME:
			response_message[1]='C';
			uint8_t timer[4];
			get_runtime_DHMS(&timer[0]);
			response_message[2]=((timer[0]%100)/10)+48;
			response_message[3]=(timer[0]%10)+48;
			response_message[4]=':';
			response_message[5]=((timer[1]%100)/10)+48;
			response_message[6]=(timer[1]%10)+48;
			response_message[7]=':';
			response_message[8]=((timer[2]%100)/10)+48;
			response_message[9]=(timer[2]%10)+48;
			response_message[10]=':';
			response_message[11]=((timer[3]%100)/10)+48;
			response_message[12]=(timer[3]%10)+48;
			response_message[13]=PROTOCOL_POSAMBLE;
			uart_driver_send(&uart_driver, (uint8_t *)response_message, 14);
			break;
		case ELEMENT_HEATER:
			response_message[1]=(char)ELEMENT_HEATER;
			response_message[2]=HAL_GPIO_ReadPin(HEATER_GPIO_Port, HEATER_Pin)+48;
			response_message[3]=PROTOCOL_POSAMBLE;
			uart_driver_send(&uart_driver, (uint8_t *)response_message, 4);
			break;
		default:
			ret_val=0;
			break;
	}
	return ret_val;
}

void parse_command(uint8_t *data)
{
	rx_packet_t *packet = (rx_packet_t *)data;
	if(packet->operation == OPERATION_GET)
	{
		if(!process_get_command(packet))
		{
			uart_driver_send(&uart_driver, (uint8_t *)nack_message, sizeof(nack_message)-1);
		}
	}else if(packet->operation == OPERATION_SET){
		if(process_set_command(packet))
		{
			uart_driver_send(&uart_driver, (uint8_t *)ack_message, sizeof(ack_message)-1);
		}else{
			uart_driver_send(&uart_driver, (uint8_t *)nack_message, sizeof(nack_message)-1);
		}
	}else{
		uart_driver_send(&uart_driver, (uint8_t *)nack_message, sizeof(nack_message)-1);
	}
}
