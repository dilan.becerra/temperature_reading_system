/**
 ****************************************************************************************************************
 * @file 	ring_buffer.c
 * @author 	Dilan Becerra (dilan.becerra@utp.edu.co)
 * @brief 	This file contains the implementation of the ring buffer class
 * @version 1.0.0
 * @date 	May 26, 2023
 ****************************************************************************************************************
 */

/** Includes --------------------------------------------------------------------------------------------------*/
#include "ring_buffer.h"

/** Private defines -------------------------------------------------------------------------------------------*/
/** Private typedef -------------------------------------------------------------------------------------------*/
/** Private macro ---------------------------------------------------------------------------------------------*/
/** Private function prototypes -------------------------------------------------------------------------------*/
/** Private variable declaration ------------------------------------------------------------------------------*/
/** Function definition ---------------------------------------------------------------------------------------*/

/**
 * @brief	This function reset the pointing ring buffer structure
 * @param 	ring_buffer: pointer to structure to be reset
 * @retval	None
 */
void ring_buffer_reset(ring_buffer_t *ring_buffer)
{
	ring_buffer->head=0;
	ring_buffer->tail=0;
	ring_buffer->full=0;
}

/**
 * @brief	This function initialize the ring buffer structure
 * @param 	ring_buffer: pointer to structure to be initialize
 * @param 	p_buffer: buffer to be managed by the ring buffer structure
 * @param 	len: size of the buffer to be managed
 * @retval	0: Error occurs (p_buffer or len are invalids)
 * @retval	1: Not errors
 */
uint8_t ring_buffer_init(ring_buffer_t *ring_buffer, uint8_t *p_buffer, size_t len)
{
	if((p_buffer==NULL)||(len==0))
	{
		return 0;
	}
	ring_buffer->buffer=p_buffer;
	ring_buffer->max=len;
	ring_buffer_reset(ring_buffer);
	return 1;
}

/**
 * @brief	Function to check if the ring buffer is full
 * @param 	ring_buffer: pointer to structure to query
 * @retval	0: Not full
 * @retval	1: Full
 */
uint8_t ring_buffer_full(ring_buffer_t *ring_buffer)
{
	return ring_buffer->full;
}

/**
 * @brief	Function to check if the ring buffer is empty
 * @param 	ring_buffer: pointer to structure to query
 * @retval	0: Not empty
 * @retval	1: Empty
 */
uint8_t ring_buffer_empty(ring_buffer_t *ring_buffer)
{
	return ((ring_buffer->full==0)&&(ring_buffer->head==ring_buffer->tail));
}

/**
 * @brief	This function returns the size of the ring buffer structure
 * @param 	ring_buffer: pointer to structure to be read
 * @return	size of the original buffer
 */
size_t ring_buffer_capacity(ring_buffer_t *ring_buffer)
{
	return ring_buffer->max;
}

/**
 * @brief	This function returns pointing to tail of ring buffer structure
 * @param 	ring_buffer: pointer to structure to be read
 * @return	pointer to ring buffer tail
 */
uint8_t *ring_buffer_fetch(ring_buffer_t *ring_buffer)
{
	return &ring_buffer->buffer[ring_buffer->tail];
}

/**
 * @brief	This function put a data in head of ring buffer structure
 * @param 	ring_buffer: pointer to structure to be modified
 * @param 	t_data: data to write
 * @retval	None
 */
void ring_buffer_set(volatile ring_buffer_t *ring_buffer, uint8_t t_data)
{
	ring_buffer->buffer[ring_buffer->head]=t_data;
	ring_buffer->head=(ring_buffer->head+1)%ring_buffer->max;
	if(ring_buffer_full(ring_buffer))
	{
		ring_buffer->tail=(ring_buffer->tail+1)%ring_buffer->max;
	}
	if(ring_buffer->head == ring_buffer->tail)
	{
		ring_buffer->full=1;
	}
}

/**
 * @brief	Function to get and exclude a data in tail of ring buffer structure
 * @param 	ring_buffer: pointer to structure to be modified
 * @param 	p_data: pointer to data to be read
 * @retval	0: ring buffer is empty, failed operation
 * @retval	1: Not empty, successful operation
 */
uint8_t ring_buffer_get(volatile ring_buffer_t *ring_buffer, uint8_t *p_data)
{
	if(!ring_buffer_empty(ring_buffer))
	{
		*p_data=ring_buffer->buffer[ring_buffer->tail];
		ring_buffer->tail=(ring_buffer->tail+1)%ring_buffer->max;
		ring_buffer->full=0;
		return 1;
	}
	return 0;
}

/**
 * @brief	Function to calculate the occupied size of ring buffer structure
 * @param 	ring_buffer: pointer to structure to be calculated
 * @return	number of occupied positions of ring buffer structure
 */
size_t ring_buffer_size(ring_buffer_t *ring_buffer)
{
	size_t size=0;
	if(ring_buffer_full(ring_buffer))
	{
		return ring_buffer->max;
	}
	if(ring_buffer->head > ring_buffer->tail)
	{
		size=ring_buffer->head - ring_buffer->tail;
	}else if(ring_buffer->head < ring_buffer->tail){
		size=ring_buffer->head + ring_buffer->max - ring_buffer->tail;
	}
	return size;
}
