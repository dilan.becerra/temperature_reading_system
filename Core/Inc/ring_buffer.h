/**
 ****************************************************************************************************************
 * @file 	ring_buffer.h
 * @author 	Dilan Becerra (dilan.becerra@utp.edu.co)
 * @brief 	This file contains the implementation of the ring buffer class
 * @version 1.0.0
 * @date 	May 26, 2023
 ****************************************************************************************************************
 */

/** Define to prevent recursive inclusions --------------------------------------------------------------------*/
#ifndef INC_RING_BUFFER_H_
#define INC_RING_BUFFER_H_

/** Includes --------------------------------------------------------------------------------------------------*/
#include "stdio.h"
#include "stdlib.h"

/** Exported defines ------------------------------------------------------------------------------------------*/
/** Exported types --------------------------------------------------------------------------------------------*/

/**
 * @struct	ring_buffer
 * @brief	Ring buffer structure definition
 */
typedef struct ring_buffer_
{
	uint8_t *buffer;
	size_t head;
	size_t tail;
	size_t max;
	uint8_t full;
}ring_buffer_t;

/** Exported constants ----------------------------------------------------------------------------------------*/
/** Exported macro --------------------------------------------------------------------------------------------*/
/** Exported functions ----------------------------------------------------------------------------------------*/
void ring_buffer_reset(ring_buffer_t *ring_buffer);
uint8_t ring_buffer_init(ring_buffer_t *ring_buffer, uint8_t *p_buffer, size_t len);
uint8_t ring_buffer_full(ring_buffer_t *ring_buffer);
uint8_t ring_buffer_empty(ring_buffer_t *ring_buffer);
size_t ring_buffer_capacity(ring_buffer_t *ring_buffer);
uint8_t *ring_buffer_fetch(ring_buffer_t *ring_buffer);
void ring_buffer_set(ring_buffer_t *ring_buffer, uint8_t t_data);
uint8_t ring_buffer_get(ring_buffer_t *ring_buffer, uint8_t *p_data);
size_t ring_buffer_size(ring_buffer_t *ring_buffer);

#endif /* INC_RING_BUFFER_H_ */
/************************************************* END OF FILE *************************************************/
