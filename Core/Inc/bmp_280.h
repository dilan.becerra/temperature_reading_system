/**
 ****************************************************************************************************************
 * @file 	bmp_280.h
 * @author 	Dilan Becerra (dilan.becerra@utp.edu.co)
 * @brief 	This file contains the functions to communicate whit a bmp380
 * @version 1.0.0
 * @date 	June 4, 2023
 ****************************************************************************************************************
 */

/** Define to prevent recursive inclusions --------------------------------------------------------------------*/
#ifndef INC_BMP_280_H_
#define INC_BMP_280_H_

#include "stm32f4xx_hal.h"

/** Includes --------------------------------------------------------------------------------------------------*/
#include <stdint.h>
#include <stdlib.h>

/** Exported defines ------------------------------------------------------------------------------------------*/
/** Exported types --------------------------------------------------------------------------------------------*/

typedef enum bmp280_register_address_{
	BMP280_REG_ADDR_ERROR				=0x00,
	BMP280_REG_ADDR_CAL_START_ADDR		=0x88,
	BMP280_REG_ADDR_ID					=0xD0,
	BMP280_REG_ADDR_RESET				=0xE0,
	BMP280_REG_ADDR_CTRL_HUM			=0xF2,
	BMP280_REG_ADDR_STATUS				=0xF3,
	BMP280_REG_ADDR_CTRL_MEAS			=0xF4,
	BMP280_REG_ADDR_CONFIG				=0xF5,
	BMP280_REG_ADDR_PRESS_MSB			=0xF7,
	BMP280_REG_ADDR_PRESS_LSB			=0xF8,
	BMP280_REG_ADDR_PRESS_XLSB			=0xF9,
	BMP280_REG_ADDR_TEMP_MSB			=0xFA,
	BMP280_REG_ADDR_TEMP_LSB			=0xFB,
	BMP280_REG_ADDR_TEMP_XLSB			=0xFC,
	BMP280_REG_ADDR_HUM_MSB				=0xFD,
	BMP280_REG_ADDR_HUM_LSB				=0xFE,
}bmp280_register_address_t;

typedef struct bmp_280_driver_{
	SPI_HandleTypeDef bmp_280_SPI;
	GPIO_TypeDef *SS_port;
	uint16_t SS_pin;
	float current_temp;
	float current_press;
}bmp_280_driver_t;

/** Exported constants ----------------------------------------------------------------------------------------*/
/** Exported macro --------------------------------------------------------------------------------------------*/
/** Exported functions ----------------------------------------------------------------------------------------*/
void bmp_280_driver_init(SPI_HandleTypeDef bmp280_SPI_com, GPIO_TypeDef *bmp280_SS_port, uint16_t bmp280_SS_pin);
float bmp_280_get_temperature();
float bmp_280_get_pressure();
void bmp_280_read();
HAL_StatusTypeDef bmp_write_register(uint8_t sensor_register, uint8_t value);
HAL_StatusTypeDef bmp280_read_register(uint8_t sensor_register, uint8_t *value);
int32_t bmp280_get_raw_TorP(uint8_t *BMPregisters);
float bmp280_convert_temperature_to_degrees(int32_t raw_temp);
float bmp280_convert_pressure_to_pascals(int32_t raw_pressure, float temp_in_degrees);

#endif /* INC_BMP_280_H_ */
/************************************************* END OF FILE *************************************************/
