/**
 ****************************************************************************************************************
 * @file 	protocol.h
 * @author 	Dilan Becerra (dilan.becerra@utp.edu.co)
 * @brief 	This file contains the communication protocol constants and structures
 * @version 1.0.0
 * @date 	May 26, 2023
 ****************************************************************************************************************
 */

/** Define to prevent recursive inclusions --------------------------------------------------------------------*/
#ifndef INC_PROTOCOL_H_
#define INC_PROTOCOL_H_

/** Includes --------------------------------------------------------------------------------------------------*/
#include <stdint.h>

/** Exported defines ------------------------------------------------------------------------------------------*/
#define PROTOCOL_PREAMBLE '*'
#define PROTOCOL_POSAMBLE '#'

/** Exported types --------------------------------------------------------------------------------------------*/

/**
 * @enum	protocol_operation
 * @brief	possibles operations in the communication protocol
 */
typedef enum protocol_operation_
{
	OPERATION_GET='G',/**< OPERATION_GET */
	OPERATION_SET='S',/**< OPERATION_SET */
}protocol_operation_t;

/**
 * @enum	protocol_element
 * @brief	possibles elements includes in the protocol
 */
typedef enum protocol_element_
{
	ELEMENT_LED='L',/**< ELEMENT_LED */
	ELEMENT_TEMPERATURE='T',
	ELEMENT_FAN_SPEED='S',
	ELEMENT_DOOR='D',
	ELEMENT_FW_VERSION='F',
	ELEMENT_UNIT_TIME='U',
	ELEMENT_HEATER='H',
	ELEMENT_FAN_STATUS='f',
}protocol_element_t;

/**
 * @struct	rx_packet
 * @brief	reception packet structure definition
 */
typedef struct rx_packet_
{
	uint8_t operation;
	uint8_t element;
	uint8_t value;
}__attribute__((__packed__))rx_packet_t;

/** Exported constants ----------------------------------------------------------------------------------------*/
/** Exported macro --------------------------------------------------------------------------------------------*/
/** Exported functions ----------------------------------------------------------------------------------------*/

#endif /* INC_PROTOCOL_H_ */
/************************************************* END OF FILE *************************************************/
