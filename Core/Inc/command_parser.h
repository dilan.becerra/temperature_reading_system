/**
 ****************************************************************************************************************
 * @file 	command_parser.h
 * @author 	Dilan Becerra (dilan.becerra@utp.edu.co)
 * @brief 	This file contains the command parser for a UART communication
 * @version 1.0.0
 * @date 	May 26, 2023
 ****************************************************************************************************************
 */

/** Define to prevent recursive inclusions --------------------------------------------------------------------*/
#ifndef INC_COMMAND_PARSER_H_
#define INC_COMMAND_PARSER_H_

/** Includes --------------------------------------------------------------------------------------------------*/
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#include "stm32f4xx_hal.h"
#include "protocol.h"

/** Exported defines ------------------------------------------------------------------------------------------*/
#define DOOR_Pin GPIO_PIN_14
#define DOOR_GPIO_Port GPIOG
#define HEATER_Pin GPIO_PIN_13
#define HEATER_GPIO_Port GPIOG

/** Exported types --------------------------------------------------------------------------------------------*/
/** Exported constants ----------------------------------------------------------------------------------------*/
/** Exported macro --------------------------------------------------------------------------------------------*/
/** Exported functions ----------------------------------------------------------------------------------------*/
void parse_command(uint8_t *data);
uint64_t get_runtime_seconds(void);
void response_temperature(void);

#endif /* INC_COMMAND_PARSER_H_ */
/************************************************* END OF FILE *************************************************/
