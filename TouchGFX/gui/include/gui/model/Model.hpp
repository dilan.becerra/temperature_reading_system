#ifndef MODEL_HPP
#define MODEL_HPP

#include <stdint.h>

class ModelListener;

class Model
{
public:
    Model();

    void bind(ModelListener* listener)
    {
        modelListener = listener;
    }

    void tick();
    void set_heater_state(uint8_t heater);
protected:
    ModelListener* modelListener;
};

#endif // MODEL_HPP
