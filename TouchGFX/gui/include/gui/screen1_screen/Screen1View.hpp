#ifndef SCREEN1VIEW_HPP
#define SCREEN1VIEW_HPP

#include <gui_generated/screen1_screen/Screen1ViewBase.hpp>
#include <gui/screen1_screen/Screen1Presenter.hpp>
#include <stdint.h>

class Screen1View : public Screen1ViewBase
{
public:
    Screen1View();
    virtual ~Screen1View() {}
    virtual void setupScreen();
    virtual void tearDownScreen();

    void updateTemp(float newTemp);
    virtual void button_toggle_led();
    virtual void update_heater_state(uint8_t heaterState){};
protected:
};

#endif // SCREEN1VIEW_HPP
