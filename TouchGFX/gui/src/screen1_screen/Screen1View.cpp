#include <gui/screen1_screen/Screen1View.hpp>

uint8_t heater_status;

Screen1View::Screen1View()
{
	heater_status=0;
}

void Screen1View::setupScreen()
{
    Screen1ViewBase::setupScreen();
}

void Screen1View::tearDownScreen()
{
    Screen1ViewBase::tearDownScreen();
}

void Screen1View::updateTemp(float newTemp)
{
	temperature_text.resizeToCurrentText();
	temperature_text.invalidate();
	//Unicode::snprintf(temperature_textBuffer, TEMPERATURE_TEXT_SIZE, "%.2f", newTemp);
	Unicode::snprintfFloat(temperature_textBuffer, TEMPERATURE_TEXT_SIZE, "%.2f", newTemp);
	temperature_text.resizeToCurrentText();
	temperature_text.invalidate();
}

void Screen1View::button_toggle_led()
{
	if(heater_status==1)
	{
		heater_status=0;
	}else{
		heater_status=1;
	}
	presenter->update_heater_state(heater_status);
}
