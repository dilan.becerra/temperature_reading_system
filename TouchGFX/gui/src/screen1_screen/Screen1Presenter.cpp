#include <gui/screen1_screen/Screen1View.hpp>
#include <gui/screen1_screen/Screen1Presenter.hpp>

Screen1Presenter::Screen1Presenter(Screen1View& v)
    : view(v)
{

}

void Screen1Presenter::activate()
{

}

void Screen1Presenter::deactivate()
{

}

void Screen1Presenter::setNewTemp(float temp)
{
	view.updateTemp(temp);
}

void Screen1Presenter::update_heater_state(uint8_t heaterState)
{
	model->set_heater_state(heaterState);
}
