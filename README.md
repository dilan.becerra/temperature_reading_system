# STM32 temperature reading system

This project has the development of a temperature measurement system and control of Boolean ports which. By connecting to an esp32 as it says in the repository https://gitlab.com/dilan.becerra/esp32fw.git you can send them to an AWS MQTT server.

## Hardware Required

The system runs on a stm32-disc1 development board, with the temperature sensor being a bmp280 connected to peripheral SPI4 and connected to ESP32 via UART1.

## How to use

To use it, just make the connections of both development boards correctly and from the AWS page you will see how the temperature is updated every 30 seconds, in the same way you can make data or firmware requests from the page.
The program also allows through touchgfx to see the temperature measured by the sensor every 5 seconds and in the same way it sends it to AWS every 30 seconds. Also through a button on the screen you can toggle one of the leds on the board.
